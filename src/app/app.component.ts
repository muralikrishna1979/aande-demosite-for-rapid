import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RequestService } from './service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',  
  styleUrls: ['./app.component.css','../assets/css/bootstrap.min.css',
  '../assets/css/style.css',
  '../assets/css/style-responsive.css',
  '../assets/css/animate.min.css',
  '../assets/css/vertical-rhythm.min.css',
  '../assets/css/owl.carousel.css',
  '../assets/css/magnific-popup.css',
  '../assets/css/rev-slider.css',
  '../assets/rs-plugin/css/settings.css',
  '../assets/css/mystyle.css'
] 
})
export class AppComponent {
  title = 'app';
}